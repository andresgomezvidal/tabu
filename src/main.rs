extern crate rand;

use std::*;
use std::fs::File;
use std::io::prelude::*;


const CIUDADES_CANTIDAD:u32=100;
const L:u32=CIUDADES_CANTIDAD-1;

fn main() {
    let argumentos: Vec<String> = env::args().collect();
    let distancias_archivo;
    let aleatorios_archivo;
//    println!("Args={:?}", argumentos);

    match argumentos.len(){                 //arg 0 es el ejecutable
        2 => {distancias_archivo=argumentos[1].to_string(); aleatorios_archivo=argumentos[0].to_string()},
        3 => {distancias_archivo=argumentos[1].to_string(); aleatorios_archivo=argumentos[2].to_string()},
        _ => {println!("Argumento 1 debe ser archivo de distancias.\nOpcionalmente argumento 2 es archivo de inicialización de aleatorios."); process::exit(1)},
    }

    let mut ciudades_lista: Vec<u32>;                                    //contiene las ciudades ordenadas
    if aleatorios_archivo==argumentos[0].to_string() {              //No hay aleatorios
        let mut vec: Vec<u32> = (1..CIUDADES_CANTIDAD).collect();    //crea el array con valores de 0 al final
        //reordena el array aleatoriamente
        use rand::{thread_rng, Rng};
        thread_rng().shuffle(&mut vec);
        ciudades_lista= vec.to_vec();
    }else{
        //lee el archivo de valores aleatorios
        let mut f = File::open(aleatorios_archivo).expect("Archivo con valores aleatorios no se puede abrir!!");
        let mut contents = String::new();
        f.read_to_string(&mut contents).expect("something went wrong reading the file");

        let mut vec: Vec<u32>=vec![0];                   //inicializa el vector con 0 para evitr aviso/error del compilador
        for line in contents.lines(){
            let mut aleatorio_val:u32= (line.parse::<f32>().unwrap() * (L as f32) + 1.0).floor() as u32;
            while aleatorio_val>=CIUDADES_CANTIDAD || vec.contains(&aleatorio_val){
                if aleatorio_val>=CIUDADES_CANTIDAD{
                    aleatorio_val=1;
                    continue;
                }
                aleatorio_val+=1;
            }
            vec.push(aleatorio_val);
        }
        vec.remove(0);                      //elimina el valor inicial que se pone para que no dea error el compilador
        ciudades_lista= vec.to_vec();
    }


    //lee el archivo de distancias
    let mut f = File::open(distancias_archivo).expect("Archivo con distancias no se puede abrir!!");
    let mut contents = String::new();
    f.read_to_string(&mut contents).expect("something went wrong reading the file");


    let matriz_distancias_size:usize=calc_matrix_size(CIUDADES_CANTIDAD);
    let mut matriz_distancias=vec![0u32;matriz_distancias_size];
    let mut cont=0;
    for line in contents.lines(){
        for i in line.split("\t"){
            matriz_distancias[cont]=i.parse::<u32>().unwrap();
            cont+=1;
        }
    }




    //HEURISTICA
    const ITERACIONES_MAX:u32 =10001;
//    const ITERACIONES_MAX:u32 =69;
//    const ITERACIONES_MAX:u32 =52;
    let mut iteraciones_reinicializacion=0;
    let mut tabu=vec![[0u32,0u32]]; tabu.pop();
    let mut mejor_solucion= ciudades_lista.to_vec();
    let mut mejor_distancia= calc_distancia_total(&mut mejor_solucion, &mut matriz_distancias);
    let mut mejor_iteracion=0;
    let mut contador_reinicializacion=0;

    println!("\
        RECORRIDO INICIAL\n\
        \tRECORRIDO: {}\n\
        \tCOSTE (km): {}\n\
        ", ciudades_lista.iter().
            fold(String::new(), |acc, e| {
                acc + &e.to_string() + " "
            })
        , mejor_distancia
    );

    for iteraciones in 1..ITERACIONES_MAX{
        if iteraciones_reinicializacion >=100 {
            iteraciones_reinicializacion=0;
            contador_reinicializacion+=1;
            ciudades_lista=mejor_solucion.to_vec();
            tabu=vec![[0u32,0u32]]; tabu.pop();
            println!("\
                ***************\n\
                REINICIO: {}\n\
                ***************\n"
            , contador_reinicializacion
            );
        }

        let mut best_temporal=u32::max_value();
        let mut bmax:u32=0;
        let mut bmin:u32=0;
        for i in 0..L{
            let mut pmax=0;
            let mut pmin=0;

            for j in 0..i {
                let max:usize;
                let min:usize;
                if i > j {
                    max = i as usize;
                    min = j as usize;
                } else {
                    max = j as usize;
                    min = i as usize;
                }
                if i == j || tabu.contains(&[max as u32, min as u32]) {
                    continue;
                }
                pmax=ciudades_lista[max];
                pmin=ciudades_lista[min];

                let distancia_permutacion: u32;
                distancia_permutacion = calc_distancia_total(
                    &mut permuta_posiciones(&mut ciudades_lista, max, min)
                    , &mut matriz_distancias);

                //ahorrar cálculos: no siempre funciona
                //if  max as u32 == L-1 ||  min<=1{
                //    distancia_permutacion = calc_distancia_total(
                //    &mut permuta_posiciones(&mut ciudades_lista, max, min)
                //    , &mut matriz_distancias);
                //}else{
                //    let neig:i32;
                //    if max-1==min{
                //        neig=0;
                //    }else{
                //        neig=- (calc_distancia(&mut matriz_distancias,pmax, ciudades_lista[max-1]) as i32)
                //        - (calc_distancia(&mut matriz_distancias,pmin, ciudades_lista[min+1]) as i32);
                //    }
                //    distancia_permutacion=(mejor_distancia as i32
                //    + neig
                //    - (calc_distancia(&mut matriz_distancias,pmax, ciudades_lista[max+1]) as i32)
                //    - (calc_distancia(&mut matriz_distancias,pmin, ciudades_lista[min-1]) as i32)
                //    + (calc_distancia(&mut matriz_distancias,pmin, ciudades_lista[max-1]) as i32)
                //    + (calc_distancia(&mut matriz_distancias,pmin, ciudades_lista[max+1]) as i32)
                //    + (calc_distancia(&mut matriz_distancias,pmax, ciudades_lista[min-1]) as i32)
                //    + (calc_distancia(&mut matriz_distancias,pmax, ciudades_lista[min+1]) as i32)
                //    ) as u32;
                //    //if iteraciones==51 && (max == 3 && min ==2) || (max == 4 && min ==2) ||(max == 4 && min ==3) {
                //    //    println!("{}",neig);
                //    //    println!("-{}",calc_distancia(&mut matriz_distancias,pmax, ciudades_lista[max+1]));
                //    //    println!("-{}",calc_distancia(&mut matriz_distancias,pmin, ciudades_lista[min-1]));
                //    //    println!("+{}",calc_distancia(&mut matriz_distancias,pmin, ciudades_lista[max-1]));
                //    //    println!("+{}",calc_distancia(&mut matriz_distancias,pmin, ciudades_lista[max+1]));
                //    //    println!("+{}",calc_distancia(&mut matriz_distancias,pmax, ciudades_lista[min-1]));
                //    //    println!("+{}",calc_distancia(&mut matriz_distancias,pmax, ciudades_lista[min+1]));
                //    //    println!("[{}, {}]={} , anterior={}, pmax={}, pmin={}",max,min,distancia_permutacion, mejor_distancia, pmax, pmin);
                //    //    println!("real={}",calc_distancia_total(
                //    //    &mut permuta_posiciones(&mut ciudades_lista, max, min)
                //    //    , &mut matriz_distancias));
//              //    //      process::exit(1);
                //    //}
                //}
                if distancia_permutacion < best_temporal
                {
                    //if distancia_permutacion!=calc_distancia_total(
                    //&mut permuta_posiciones(&mut ciudades_lista, max, min)
                    //, &mut matriz_distancias){
                    //    println!("NO IGUALES ({},{}): {} != {}",max,min,distancia_permutacion,
                    //        calc_distancia_total(
                    //    &mut permuta_posiciones(&mut ciudades_lista, max, min)
                    //        , &mut matriz_distancias));
                    //}
                    best_temporal=distancia_permutacion;
                    bmax=max as u32;
                    bmin=min as u32;
                }

            }
        }

        tabu.push([bmax,bmin]);
        if tabu.len() > (L+1) as usize{
            tabu.remove(0);
        }

        ciudades_lista=permuta_posiciones(&mut ciudades_lista, bmax as usize, bmin as usize);
        if best_temporal < mejor_distancia{
            mejor_solucion=ciudades_lista.to_vec();
            mejor_distancia=best_temporal;
            mejor_iteracion=iteraciones;
            iteraciones_reinicializacion=0;
        }else{
            iteraciones_reinicializacion+=1;
        }


        //por eficiencia
        let tmp_str=format!("\
            ITERACION: {}\n\
            \tINTERCAMBIO: ({}, {})\n\
            \tRECORRIDO: {}\n\
            \tCOSTE (km): {}\n\
            \tITERACIONES SIN MEJORA: {}\n\
            \tLISTA TABU:\n{}\
            ", iteraciones
            , bmax , bmin
            , ciudades_lista.iter().
            fold(String::new(), |acc, e| {
                acc + &e.to_string() + " "
            })
            , best_temporal
            , iteraciones_reinicializacion
            , tabu.iter()
                 .fold(String::new(), |acc, &[i, j]| {
                     acc + "\t" + &i.to_string() + " " + &j.to_string() + "\n"
                 })
        );
        println!("{}",tmp_str);
    }

    println!("\
        \nMEJOR SOLUCION: \n\
        \tRECORRIDO: {}\n\
        \tCOSTE (km): {}\n\
        \tITERACION: {}\
        ", mejor_solucion.iter().
            fold(String::new(), |acc, e| {
                acc + &e.to_string() + " "
           })
        , mejor_distancia
        , mejor_iteracion
    );
}

fn permuta_posiciones(solucion: &mut [u32], i:usize, j:usize) -> Vec<u32> {
    let mut permutado= solucion.to_vec();
    permutado[i]=solucion[j];
    permutado[j]=solucion[i];

    permutado
}
fn calc_matrix_index(i:u32, j:u32) -> usize {
    ((i + 1) * i / 2 + j) as usize
}

fn calc_distancia_inicio_fin(solucion: &mut [u32], matriz_distancias: &mut [u32]) -> u32{
    calc_distancia(matriz_distancias, 0, solucion[0])
        + calc_distancia(matriz_distancias, 0, solucion[(solucion.len()-1) as usize])
}
fn calc_distancia_interna(solucion: &mut [u32], matriz_distancias: &mut [u32]) -> u32{
    let mut distancia_total: u32= 0;
    let max:usize=solucion.len()-1;
    let mut i:usize=0;
    while i< max{
        distancia_total += calc_distancia(matriz_distancias, solucion[i], solucion[i+1]);
        i+=1;
    }
    distancia_total
}
fn calc_distancia_total(solucion: &mut [u32], matriz_distancias: &mut [u32]) -> u32{
    let mut distancia_total: u32= calc_distancia_inicio_fin(solucion, matriz_distancias);
    let max:usize=solucion.len()-1;
    let mut i:usize=0;
    while i< max{
        distancia_total += calc_distancia(matriz_distancias, solucion[i], solucion[i+1]);
        i+=1;
    }
    distancia_total
}
fn calc_distancia(matriz_distancias: &mut [u32], i:u32, j:u32) -> u32{
    let row;
    let col;
    if i>j{
        row=i;
        col=j;
    }else if j>i {
        row=j;
        col=i;
    }else{
        return 0
    }

    matriz_distancias[calc_matrix_index(row-1,col)]
}


fn calc_matrix_size(max:u32) -> usize{
    let mut tam:u32=0;
    for i in 1..max{
        tam+=i;
    }
    return tam as usize
}
