#!/usr/bin/env bash


salir_f(){
	echo "Error $? al ejecutar el comando anterior"
	exit 1
}

if [ ! -d ~/.cargo ]; then
	echo "Es necesario descargar e 'instalar' el compilador de Rust en ~/.cargo, ~/.rustup y ~/.multirust (slink .multirust -> .rustup)"
	echo "Se adjunta desinstalar.sh, por si fuera de interés"
	{
		curl https://sh.rustup.rs -sSf > RustInst.sh &&
		sh RustInst.sh -y --no-modify-path > /dev/null &&
		rm RustInst.sh
	} || salir_f
fi

nombre=`awk -F '=' '$1~/name/ {print $2}' Cargo.toml |awk -F '"' '{print $2}'`

. ~/.cargo/env
cargo build --release
cp target/release/"$nombre" ./a.out
